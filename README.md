﻿# Z_Shell
一个基于java编写的模拟shell
##说明文档 
 Z_Shell Manual 
                 命令指南
 ls 查看当前目录下的所有文件
 cd <path> 进入当前目录下的path相对路径 或进入系统下的path绝对路径
 cd..	回退到父目录下
 cat <file_name> 查看命令 显示"文本信息
 exit 退出Z_Shell
 version Z_Shell 版本信息
##版本信息
  Z_Shell Version 
  version: v 1.0
  
  date   : 16/06/19
  
  author : YueNing Zhao
  
  e_mail : zhaozhaozhao1996@163.com
  
  此次指令集
    ls、cd、cd..、cat、exit、version、--help & -h
  
  后续将添加指令
    mkdir copy cut rm  
 
## 演示
### 使用java -jar 运行 根目录下的run.jar包
![输入图片说明](http://git.oschina.net/uploads/images/2016/0619/184406_f327156b_451432.png "在这里输入图片标题")
### ls命令
![输入图片说明](http://git.oschina.net/uploads/images/2016/0619/184417_3a136ffc_451432.png "在这里输入图片标题")
### cd命令
![输入图片说明](http://git.oschina.net/uploads/images/2016/0619/184427_266bf162_451432.png "在这里输入图片标题")
### cat命令
![输入图片说明](http://git.oschina.net/uploads/images/2016/0619/184326_611a03ca_451432.png "在这里输入图片标题")
### 使用帮助查看说明
![输入图片说明](http://git.oschina.net/uploads/images/2016/0619/184440_ff90b6ef_451432.png "在这里输入图片标题")

